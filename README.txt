### Description du projet

"Le chiffre clé" est un programme qui permet de décrypter un message chiffré par la méthode de Vigenère sans connaître la clé. Cette dernière est une chaîne de caractères qui est répétée sous chaque caractère du message crypté. Chaque caractère de la clé est interprété en décalage selon sa position dans l’alphabet (A=0, B=1, C=2, etc.). En somme, notre projet trouve la clé et décrypte le message. Ainsi, l’utilisateur n’a qu’à indiquer le message à décrypter.


### Protocole d'utilisation 

Pour pouvoir utiliser le programme :
- Allez dans le dossier "Sources" : \LeChiffreCle\sources
- Ouvrez le fichier "messageCrypte.txt"
- Modifiez le texte en remplaçant le message crypté par celui chiffré avec la méthode de Vigenère.
- Enregistrez le fichier "messageCrypte.txt"
- Lancez et exécutez le fichier "trouveCle.py"

Vous pouvez essayer le programme en utilisant un message crypté par ce site :
il s'agit d'un site utilisé afin de pouvoir créer un message crypté grace à un texte et une clé :

https://www.dcode.fr/chiffre-vigenere

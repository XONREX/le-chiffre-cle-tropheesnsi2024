###############################################################
#                         Fonctions                           #
###############################################################

def importerMessage():
    """
      Cette fonction permet d'importer le message crypté dans le programme pour le définir à la variable messageCrypte.

      :return: messageCrypte (str)
      """

    with open("messageCrypte.txt") as messageCrypte_txt:                                                                # On ouvre le fichier messageCrypte comme messageCrypte_txt.
        messageCrypte = messageCrypte_txt.read()                                                                        # On définit la variable messageCrypte à la chaîne de caractères lue dans le fichier messageCrypte.txt.

    return messageCrypte

def enleveCaracteresSpeciaux(messageCrypte):
    """
      Cette fonction permet de retirer les caractères spéciaux ((espace)!#%&? etc.) du message crypté par
      l'utilisateur.

      :param messageCrypte: message crypté par l'utilisateur (str)
      :return: message crypté sans caractère spécial (str)
      """

    for caractere in messageCrypte:
        if not caractere.isalpha():                                                                                     # On teste si le caractère n'est pas un caractère de l'alphabet.
            messageCrypte = messageCrypte.replace(caractere, "")                                                        # Remplacement des caractères différents de l'alphabet par des caractères vides afin de supprimer les caractères spéciaux.

    return messageCrypte

def intervalle(messageCrypte, tailleCle):
    """
      Cette fonction permet de créer un dictionnaire qui comprend en clé les décalages de 0 jusqu'à tailleCle
      et en valeur le message crypté décalé. C'est-à-dire pour un décalage de 2, la valeur est une chaîne de
      caractères comprenant la première lettre, la troisième, la cinquième, la septième, etc. (de 2 en 2).

      :param messageCrypte: message crypté (str)
      :param tailleCle: taille de la clé (int)
      :return: retourne le dictionnaire du message crypté selon la taille de la clé (dict)
      """

    messageCrypteDictionnaire = {}                                                                                      # Création du dictionnaire vide qui comprendra en clé le nombre de décalage et en valeur le message crypté décalé.
    for debutDecalage in range(tailleCle):                                                                              # Boucle qui parcourt tous les décalages possibles de 0 jusqu'en tailleCle.
        texteDecale = ""                                                                                                # Création d'une chaîne de caractères vide qui contiendra le message crypté décalé.

        for index in range(debutDecalage, len(messageCrypte), tailleCle):                                               # Boucle qui va de debutDecalage jusqu'à la fin du message crypté de tailleCle en taille de clé (par exemple, de 5 en 5 si la clé est "POEME").
            texteDecale += messageCrypte[index]                                                                         # Ajout de la lettre du message crypté décalé à la chaîne de caractères texteDecale.

        messageCrypteDictionnaire[debutDecalage] = texteDecale                                                          # Ajout de la clé (0 jusqu'à tailleCle) et la valeur (chaîne de caractères du message crypté de tailleCle en taille de clé).

    return messageCrypteDictionnaire

def indiceIC(messageCrypteDictionnaire):
    """
    Cette fonction permet de trouver l'indice de coincidence des différents messages cryptés décalés.

    :param messageCrypteDictionnaire: dictionnaire du message crypté selon le nombre de décalage (dict)
    :return: retourne l'indice de coincidence pour chaque message crypté décalé / la valeur se nomme ic_parDecalage
    """

    ic_parDecalage = {}                                                                                                 # Dictionnaire pour stocker les indices de coïncidence pour chaque décalage.

    for decalage in range(len(messageCrypteDictionnaire)):                                                              # Boucle à travers chaque décalage possible.

        nombreRecurrence = {}                                                                                           # Dictionnaire pour compter les récurrences de chaque caractère.
        indiceCoincidence = {}                                                                                          # Dictionnaire pour stocker les indices de coïncidence de chaque caractère.

        for caractere in messageCrypteDictionnaire[decalage]:                                                           # Boucle à travers chaque caractère dans le message crypté.
            if caractere in nombreRecurrence:
                nombreRecurrence[caractere] += 1                                                                        # Augmente le nombre de récurrences du caractère de 1.
            else:
                nombreRecurrence[caractere] = 1                                                                         # Initialise le nombre de récurrences du caractère à 1 s'il est nouveau.

            diviseur = len(messageCrypteDictionnaire[decalage]) * (len(messageCrypteDictionnaire[decalage]) - 1)        # Calcule le diviseur pour l'indice de coïncidence.

            if diviseur != 0:
                for caractere in nombreRecurrence:
                    indiceCoincidence[caractere] = (nombreRecurrence[caractere] *
                                                    (nombreRecurrence[caractere] - 1)) / diviseur                       # Calcule l'indice de coïncidence pour chaque caractère (formule pour calculer l'ic).
            else:
                indiceCoincidence[caractere] = 0                                                                        # Si le diviseur est 0, l'indice de coïncidence est défini à 0.

        ic = 0                                                                                                          # Initialise l'indice de coïncidence total à 0.
        for cle in indiceCoincidence.keys():
            ic += indiceCoincidence[cle]                                                                                # Ajoute l'indice de coïncidence de chaque caractère à l'indice total.
            ic_parDecalage[decalage] = ic                                                                               # Stocke l'indice de coïncidence total pour le décalage actuel.
    return ic_parDecalage                                                                                               # Retourne le dictionnaire des indices de coïncidence pour chaque décalage.


def trouveTailleCle(messageCrypte):
    """
      Cette fonction permet de trouver la taille de la clé grâce aux fonctions précédentes.

      :param messageCrypte: message crypté (str)
      :return: retourne la taille de la clé (int)
    """
    nombreDecalage = 1                                                                                                  # Initialisation de la variable qui va contenir le nombre de décalage.
    ic_parDecalage = indiceIC(intervalle(messageCrypte, nombreDecalage))                                                # Appel de la fonction indiceIC pour trouver l'indice de coincidence pour chaque message crypté décalé.
    moyenneIC = 0                                                                                                       # Initialisation de la variable qui va contenir la moyenne des IC.
    tailleCle = 0                                                                                                       # Initialisation de la variable qui va contenir la taille de la clé.

    while moyenneIC < 0.06:                                                                                             # Tant que la moyenne des IC est inférieure à 0.06 (environ moyenne française 0,074), on continue à augmenter le nombre de décalage.
        sommeIC = 0
        for valeuric_parDecalage in ic_parDecalage.values():                                                            # Boucle qui parcourt les valeurs de l'indice de coincidence pour chaque décalage.
            sommeIC += valeuric_parDecalage                                                                             # Ajout de la valeur de l'indice de coincidence à la somme des IC pour pouvoir calculer la moyenne des IC de chaque décalage pour avoir une valeur plus précise.
        moyenneIC = sommeIC / len(ic_parDecalage)                                                                       # Calcul de la moyenne.

        nombreDecalage += 1                                                                                             # Ajout de 1 au nombre de décalage pour pouvoir continuer à augmenter le nombre de décalage.
        tailleCle += 1                                                                                                  # Ajout de 1 à la taille de la clé pour pouvoir continuer à augmenter la taille de la clé.

        ic_parDecalage = indiceIC(intervalle(messageCrypte, nombreDecalage))
    return tailleCle


def occurenceMessageDecoupe(messageDecoupe):
    """
      Cette fonction permet de créer un dictionnaire selon le décalage (en clé) de dictionnaires de l'occurrence de
       chaque caractère (on a un dictionnaire de dictionnaires).

      :param messageDecoupe: dictionnaire du message crypté selon le nombre décalage (cf : fonction intervalle) (dict)
      :return: dictionnaire de dictionnaires de l'occurrence des caractères (dict)
    """

    OccurrenceParDecalage = {}                                                                                          # Création du dictionnaire vide qui contiendra pour chaque décalage un dictionnaire avec chaque lettre et son occurrence de chaque messages découpés.

    for decalage in messageDecoupe.keys():                                                                              # Boucle qui parcourt tous les décalages possibles.
        nombreRecurrence = {}                                                                                           # Création du dictionnaire vide qui contiendra pour chaque lettre leur occurrence.

        for caractere in messageDecoupe[decalage]:                                                                      # Boucle qui parcourt tous les caractères du message crypté découpé.
            if caractere in nombreRecurrence:
                nombreRecurrence[caractere] += 1
            else:
                nombreRecurrence[caractere] = 1
        OccurrenceParDecalage[decalage] = nombreRecurrence

    return OccurrenceParDecalage


def occurencecaractereMax(OccurrenceParDecalage):
    """
      Cette fonction crée un dictionnaire de dictionnaires de la lettre qui apparaît le plus souvent,
       selon le décalage

      :param OccurrenceParDecalage: dictionnaire de dictionnaires de l'occurrence des caractères (dict)
      :return: retourne un dictionnaire (dict)
      """

    lettreMaxOccurrence = {}
    for decalage in OccurrenceParDecalage.keys():                                                                       # Boucle qui parcourt tous les décalages possibles dans le dictionnaire OccurrenceParDecalage qui regroupe plusieurs dictionnaires.
        occurrenceMaxDecalage = 0

        for caractere, valeur in OccurrenceParDecalage[decalage].items():                                               # On récupère les clés et les valeurs des dictionnaires du dictionnaire OccurrenceParDecalage.

            if valeur > occurrenceMaxDecalage:
                lettreMaxOccurrence[decalage] = {caractere: valeur}                                                     # On ajoute au dictionnaire lettreMaxOccurrence un dictionnaire de la clé et la valeur de la lettre qui apparaît le plus souvent.
                occurrenceMaxDecalage = valeur                                                                          # On assigne à occurrenceMaxDecalage la récurrence de la lettre qui apparaît le plus souvent.
    return lettreMaxOccurrence


def trouveDecalage(lettreMaxOccurrence, alphabet):
    """
      Cette fonction crée un dictionnaire qui donne l'éloignement de la lettre qui apparaît le plus de fois,
       par rapport à la position de la lettre E (la lettre qui apparaît le plus souvent dans l'alphabet français)

      :param lettreMaxOccurrence: dictionnaire de dictionnaires de la lettre qui apparaît le plus souvent,
      selon le décalage (dict)
      :param alphabet: chaîne de caractères qui comprend toutes les lettres de l'alphabet (str)
      :return: retourne le dictionnaire qui donne la distance entre la lettre et E
    """

    indexE = alphabet.index("E")                                                                                        # Trouve la position de la lettre E dans l'alphabet.
    decalageAlphabet = {decalage: alphabet.index(list(lettreMaxOccurrence[decalage].keys())[0]) - indexE for decalage
                        in lettreMaxOccurrence.keys()}                                                                  # Création du dictionnaire qui donne la distance entre la lettre et E et la lettre la plus utilisée de chaque décalage (valeur de lettreMaxOccurrence)
    return decalageAlphabet


def Cle(alphabet, decalageAlphabet):
    """
      Cette fonction permet de trouver la clé qui a crypté le message sans connaître cette clé.

      :param alphabet: chaîne de caractères des lettres de l'alphabet français (str)
      :param decalageAlphabet: dictionnaire de la distance en index par rapport à la lettre E,
      selon les décalages (dict)
      :return: retourne la clé (str)
    """

    cle = ""
    for decalageLettreAlphabet in decalageAlphabet.values():                                                            # Boucle qui parcourt tous les décalages de lettre de l'alphabet.
        indexLettreAlphabet = decalageLettreAlphabet % 26                                                               # Trouve l'index de decalageLettreAlphabet dans l'alphabet.
        cle += alphabet[indexLettreAlphabet]                                                                            # Ajoute la lettre de l'alphabet à la clé.
    return cle

def trouveCle(messageCrypte, tailleCle, alphabet):
    """
      Cette fonction regroupe toutes les fonctions présentées précédemment

      :param messageCrypte: chaîne de caractères du message crypté par une clé que l'on ne connaît pas. (str)
      :param tailleCle: longueur de la clé (int)
      :return: retourne rien
    """

    messageDecoupe = intervalle(messageCrypte, tailleCle)                                                               # On découpe le message crypté selon la taille de la clé (les décalages).
    occurrence_parDecalage = occurenceMessageDecoupe(messageDecoupe)                                                    # On trouve l'occurrence de chaque caractère selon les décalages.
    lettreMaxOccurrence = occurencecaractereMax(occurrence_parDecalage)                                                 # On trouve la lettre qui apparaît le plus souvent pour chaque décalage.
    decalageAlphabet = trouveDecalage(lettreMaxOccurrence, alphabet)                                                    # On trouve la distance entre la lettre qui apparaît le plus souvent et la lettre E.
    cle = Cle(alphabet, decalageAlphabet)                                                                               # On trouve la clé grâce la distance entre la lettre qui apparaît le plus souvent et la lettre E est l'index de la lettre dans l'alphabet.

    return cle
def decrypte(messageCrypte, cle, alphabet):
    """
        Cette fonction permet de décrypter le message crypté.
        :param messageCrypte: message crypté (str)
        :param cle: clé (str)
        :param alphabet: chaîne de caractères des lettres de l'alphabet français
    """
    messageDecrypte = ""                                                                                                # Création d'une chaîne de caractères vide qui contiendra le message décrypté.

    for index, caractere in enumerate(messageCrypte):
        indexMessage = alphabet.index(caractere.upper())                                                                # Trouver l'index du caractère dans l
        indexCle = alphabet.index(cle[index % len(cle)])                                                                # Trouver l'index de la lettre de la clé correspondant au caractère actuel
        indexDecrypte = (indexMessage - indexCle) % 26                                                                  # Calculer l'index du caractère décrypté
        messageDecrypte += alphabet[indexDecrypte]                                                                      # Ajouter le caractère décrypté au message décrypté

    return messageDecrypte


###############################################################
#                         Code(main)                          #
###############################################################

messageCrypte = importerMessage()

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"                                                                                 # On définit l'alphabet français.

messageCrypte = enleveCaracteresSpeciaux(messageCrypte)                                                                 # Lancement de la fonction pour enlever les caractères spéciaux du message codé
tailleCle = trouveTailleCle(messageCrypte)                                                                              # Lancement de la fonction pour trouver la taille de la clé
cle = trouveCle(messageCrypte, tailleCle, alphabet)                                                                     # Lancement de la fonction pour définir la clé

print("La taille de la clé est :", tailleCle)
print("La clé est :", cle)
print("Le message décrypté est :", decrypte(messageCrypte, cle, alphabet))
